FROM openjdk:8-jdk-alpine
MAINTAINER com.nisum.test
COPY build/libs/testNisum-0.0.1-SNAPSHOT.jar testNisum-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/testNisum-0.0.1-SNAPSHOT.jar"]