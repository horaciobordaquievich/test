# README #

Para compilar la aplicaci�n pararse en la raiz del proyecto y ejecutar:

``gradlew clean build``

Para levantar la aplicaci�n ir a /build/libs/ y ejecutar:

``java -jar testNisum-0.0.1-SNAPSHOT.jar``

---

Si se quiere crear una imagen Docker y luego levantar un container, pararse en la raiz del proyecto y correr:

``docker build --tag=nisum-test:latest .`` 

``docker run -p 8080:8080 nisum-test:latest`` 

Para levantar un container y configurar variables de entorno se puede modificar el archivo env.list y correr:

``docker run -p 8080:8080 --env-file env.list nisum-test:latest`` 

_Para cualquier forma que se levante el container es necesario determinar en que puerto se quiere levantar la aplicaci�n_

## Para consumir los servicios con Swagger


Ir a la siguiente direcci�n:

``http://localhost:8080/nisumTest/swagger-ui.html#``

1) Ejecutar el servicio de login
![alt text](readmeFiles/swagger1.JPG "Controller de Login")
2) Copiar el token para autenticarse
![alt text](readmeFiles/swagger2.JPG "Controller de Login")
3) Autenticar la API con el token
![alt text](readmeFiles/swagger3.JPG "Autenticaci�n")
4) Consumir el servicio
![alt text](readmeFiles/swagger4.JPG "Controller de Usuario")
![alt text](readmeFiles/swagger5.JPG "Controller de Usuario")
