
create table NISUM_USER
(
	ID varchar(255) not null,
	NAME VARCHAR(255) not null,
	CREATED DATETIME2 not null,
	MODIFIED DATETIME2,
	TOKEN VARCHAR(max),
	LAST_LOGIN DATETIME2 not null,
	IS_ACTIVE BOOLEAN,
	EMAIL VARCHAR(255) not null
);

create unique index NISUM_USER_EMAIL_UINDEX
	on NISUM_USER (EMAIL);

create unique index NISUM_USER_ID_UINDEX
	on NISUM_USER (ID);

alter table NISUM_USER
	add constraint NISUM_USER_PK
		primary key (ID);

create table PHONE
(
	ID varchar(255) not null,
	ID_NISUM_USER varchar(255) not null,
	PHONE_NUMBER VARCHAR(30),
	CITY_CODE VARCHAR(10),
	COUNTRY_CODE VARCHAR(10),
	constraint PHONE_NISUM_USER_ID_FK
		foreign key (ID_NISUM_USER) references NISUM_USER (ID)
);

create unique index PHONE_ID_UINDEX
	on PHONE (ID);

alter table PHONE
	add constraint PHONE_PK
		primary key (ID);


