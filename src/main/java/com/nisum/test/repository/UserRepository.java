package com.nisum.test.repository;

import com.nisum.test.model.NisumUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

/**
 * User Repository
 */
@Repository
public interface UserRepository extends JpaRepository<NisumUser, UUID> {
    /**
     *
     * @param email to search
     * @return Optional of {@link NisumUser}
     */
    Optional<NisumUser> findByEmail(String email);
}
