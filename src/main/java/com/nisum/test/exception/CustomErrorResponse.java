package com.nisum.test.exception;

import lombok.Data;

/**
 * Custom Error Message to show an error
 */
@Data
public class CustomErrorResponse {
    private String message;
}
