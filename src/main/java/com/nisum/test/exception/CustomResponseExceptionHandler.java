package com.nisum.test.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Custom Handler for Response Exceptions
 */
@ControllerAdvice
public class CustomResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return new ResponseEntity<>(getCustomErrorResponse(ex), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UnauthorizedUserException.class)
    public ResponseEntity<CustomErrorResponse> customUnauthorizedUserError(Exception ex) {

        return new ResponseEntity<>(getCustomErrorResponse(ex), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<CustomErrorResponse> customHandleNotFound(Exception ex) {

        return new ResponseEntity<>(getCustomErrorResponse(ex), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<CustomErrorResponse> customHandleBadRequest(Exception ex) {

        return new ResponseEntity<>(getCustomErrorResponse(ex), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InternalServerErrorException.class)
    public ResponseEntity<CustomErrorResponse> customHandleInternalServerError(Exception ex) {

        return new ResponseEntity<>(getCustomErrorResponse(ex), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private CustomErrorResponse getCustomErrorResponse (Exception exception){
        CustomErrorResponse errors = new CustomErrorResponse();
        errors.setMessage(exception.getMessage());

        return errors;
    }

}
