package com.nisum.test.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Entity Not Found Exception
 */
@ResponseStatus(value=HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException{
    public EntityNotFoundException(){
        super();
    }

    /**
     * @param message Message to display
     */
    public EntityNotFoundException(String message) {
        super(message);
    }

    /**
     * @param cause Throwable cause of the Exception
     */
    public EntityNotFoundException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message Message to display
     * @param cause Throwable cause of the Exception
     */
    public EntityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message Message to display
     * @param cause Throwable cause of the Exception
     * @param enableSuppression if true suppression is enabled
     * @param writableStackTrace if true write stacktrace
     */
    public EntityNotFoundException(String message, Throwable cause, boolean enableSuppression,
                                   boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
