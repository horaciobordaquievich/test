package com.nisum.test.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Unauthorized User Exception
 */
@ResponseStatus(value=HttpStatus.UNAUTHORIZED)
public class UnauthorizedUserException extends RuntimeException{
    public UnauthorizedUserException(){
        super();
    }
    /**
     * @param message Message to display
     */
    public UnauthorizedUserException(String message) {
        super(message);
    }

    /**
     * @param cause Throwable cause of the Exception
     */
    public UnauthorizedUserException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message Message to display
     * @param cause Throwable cause of the Exception
     */
    public UnauthorizedUserException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message Message to display
     * @param cause Throwable cause of the Exception
     * @param enableSuppression if true suppression is enabled
     * @param writableStackTrace if true write stacktrace
     */
    public UnauthorizedUserException(String message, Throwable cause, boolean enableSuppression,
                                     boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
