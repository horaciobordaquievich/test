package com.nisum.test.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Internal Server Error Exception
 */
@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalServerErrorException extends RuntimeException{
    public InternalServerErrorException(){
        super();
    }

    /**
     * @param message Message to display
     */
    public InternalServerErrorException(String message) {
        super(message);
    }

    /**
     * @param cause Throwable cause of the Exception
     */
    public InternalServerErrorException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message Message to display
     * @param cause Throwable cause of the Exception
     */
    public InternalServerErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     *            Message to display
     * @param cause
     *            Throwable cause of the Exception
     * @param enableSuppression
     *            if true suppression is enabled
     * @param writableStackTrace
     *            if true write stacktrace
     */
    public InternalServerErrorException(String message, Throwable cause, boolean enableSuppression,
                                        boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
