package com.nisum.test.jwt;

import com.nisum.test.exception.UnauthorizedUserException;
import com.nisum.test.spi.response.TokenResponse;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Data;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Data
@Component
public class JWTTokenManager {
    public static final String HEADER = "Authorization";
    public static final String PREFIX = "Bearer ";

    public TokenResponse getJWTToken(String username) {
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");
        Date expirationDate = new Date(System.currentTimeMillis() + 60L * 1000 * JWTConfigs.TOKEN_EXPIRATION_DATE);
        Date issuedAt = new Date(System.currentTimeMillis());
        String token = Jwts
                .builder()
                .setIssuer("nisum.com.JWT")
                .setId(UUID.randomUUID().toString())
                .setSubject(username)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512,
                        JWTConfigs.SECRET_KEY.getBytes()).compact();

        String refreshToken = Jwts.builder()
                .claim("authorities_refresh_token",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setSubject(username)
                .setIssuer("nisum.com.JWT")
                .setId(UUID.randomUUID().toString())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 60L * 1000 * JWTConfigs.REFRESH_TOKEN_EXPIRATION_DATE))
                .signWith(SignatureAlgorithm.HS512,
                        JWTConfigs.SECRET_KEY.getBytes())
                .compact();

        TokenResponse tokenResponse = new TokenResponse();

        tokenResponse.setToken(token);
        tokenResponse.setExpirationDate(expirationDate);
        tokenResponse.setIssuedAt(issuedAt);
        tokenResponse.setTokenType(JWTConfigs.BEARER);
        tokenResponse.setRefreshToken(refreshToken);

        return tokenResponse;
    }
    public String getUserFromRefreshToken(String refreshToken){
        Claims claims = Jwts.parser().setSigningKey(JWTConfigs.SECRET_KEY.getBytes()).parseClaimsJws(refreshToken).getBody();
        if (claims.get("authorities_refresh_token") != null) {
            return claims.getSubject();
        } else {
            SecurityContextHolder.clearContext();
            throw new UnauthorizedUserException("refreshToken not valid");
        }

    }
    public static Claims validateToken(HttpServletRequest request) {
        String jwtToken = request.getHeader(HEADER).replace(PREFIX, "");
        return Jwts.parser().setSigningKey(JWTConfigs.SECRET_KEY.getBytes()).parseClaimsJws(jwtToken).getBody();
    }

    /**
     * Metodo para autenticarnos dentro del flujo de Spring
     *
     * @param claims
     */
    public static void setUpSpringAuthentication(Claims claims) {
        @SuppressWarnings("unchecked")
        List<String> authorities = (List) claims.get("authorities");

        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(claims.getSubject(), null,
                authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
        SecurityContextHolder.getContext().setAuthentication(auth);

    }

    public static boolean validJWTToken(HttpServletRequest request) {
        String authenticationHeader = request.getHeader(HEADER);
        if (authenticationHeader == null || !authenticationHeader.startsWith(PREFIX))
            return false;
        return true;
    }
}
