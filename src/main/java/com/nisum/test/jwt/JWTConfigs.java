package com.nisum.test.jwt;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class JWTConfigs {
    public static final String BEARER = "Bearer";
    public static String SECRET_KEY;
    public static Integer TOKEN_EXPIRATION_DATE;
    public static Integer REFRESH_TOKEN_EXPIRATION_DATE;

    @Value("${security.jwt.secret-key}")
    public void setSecretKey(String secretKey){
        JWTConfigs.SECRET_KEY = secretKey;
    }

    @Value("${security.jwt.token-expiration-time}")
    public void setExpirationTime(Integer expirationTime){
        JWTConfigs.TOKEN_EXPIRATION_DATE = expirationTime;
    }

    @Value("${security.jwt.refresh-token-expiration-time}")
    public void setRefreshTokenExpirationDatey(Integer refreshTokenExpirationDate){
        JWTConfigs.REFRESH_TOKEN_EXPIRATION_DATE = refreshTokenExpirationDate;
    }
}
