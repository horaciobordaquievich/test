package com.nisum.test.mapping;

import com.nisum.test.model.Phone;
import com.nisum.test.spi.request.PhoneRequest;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * Mapper for {@link com.nisum.test.model.NisumUser}
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PhoneMapper {

    Phone phoneRequestToPhone(PhoneRequest phoneRequest);
    List<Phone> phoneRequestListToPhoneList(List<PhoneRequest> phoneRequestList);
}
