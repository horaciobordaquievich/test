package com.nisum.test.mapping;

import com.nisum.test.model.NisumUser;
import com.nisum.test.spi.request.UserRequest;
import com.nisum.test.spi.response.UserResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * Mapper for {@link com.nisum.test.model.NisumUser}
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface NisumUserMapper {

    NisumUser userRequestToNisumUser(UserRequest userRequest);

    @Mappings({
            @Mapping(target = "last_login", source = "lastLogin"),
            @Mapping(target = "isactive", source = "active")
    })
    UserResponse nisumUserToUserResponse(NisumUser nisumUser);

    List<UserResponse> nisumUserListToUserResponseList(List<NisumUser> nisumUsers);
}
