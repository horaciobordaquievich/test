package com.nisum.test.controller;

import com.nisum.test.service.UserService;
import com.nisum.test.spi.request.UserRequest;
import com.nisum.test.spi.response.UserResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * REST Endpoints
 */
@RestController
@RequestMapping("/users")
@Api("User Endpoints")
@Data
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Create a User", response = UserResponse.class)
    public ResponseEntity<UserResponse> createUser(@Valid @RequestBody UserRequest userRequest,@ApiParam(hidden = true) @RequestHeader(name="Authorization", required = false) String token) {

        return ResponseEntity.ok(userService.createUser(userRequest,token));
    }


    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "List all users", response = UserResponse.class)
    public ResponseEntity<List<UserResponse>> listUsers() {

        return ResponseEntity.ok(userService.listUsers());
    }


}
