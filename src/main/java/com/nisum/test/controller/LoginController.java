package com.nisum.test.controller;

import com.nisum.test.exception.UnauthorizedUserException;
import com.nisum.test.jwt.JWTTokenManager;
import com.nisum.test.service.UserService;
import com.nisum.test.spi.request.LoginRequest;
import com.nisum.test.spi.request.RefreshTokenRequest;
import com.nisum.test.spi.response.TokenResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Endpoints for login
 */
@RestController
@RequestMapping("/")
@Api("Auth Endpoints")
@Data
@Validated
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private JWTTokenManager jwtTokenManager;

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Validate the user, if is valid return an access token", response = String.class)
    public ResponseEntity<?> login(@RequestBody LoginRequest loginRequest) {
        if(!userService.validateUser(loginRequest)) throw new UnauthorizedUserException("The username or password is incorrect");
        TokenResponse token = getJwtTokenManager().getJWTToken(loginRequest.getUsername());

        return ResponseEntity.ok(token);
    }

    @PostMapping(value = "/refreshToken", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Validate the user with a refreshToken, if is valid return a new access token", response = String.class)
    public ResponseEntity<?> refreshToken(@RequestBody RefreshTokenRequest refreshTokenRequest) {
        String username = getJwtTokenManager().getUserFromRefreshToken(refreshTokenRequest.getRefreshToken());

        TokenResponse token = getJwtTokenManager().getJWTToken(username);

        return ResponseEntity.ok(token);
    }
}
