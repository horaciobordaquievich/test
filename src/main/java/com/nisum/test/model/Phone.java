package com.nisum.test.model;

import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

/**
 * Phone
 */
@Data
@Entity
@Table(name = "phone")
public class Phone {
    /**
     * Phone Id
     */
    @Id
    @Column(name = "id")
    @Type(type="uuid-char")
    @GeneratedValue
    private UUID id;

    /**
     * Nisum User
     */
    @ManyToOne(targetEntity = NisumUser.class, fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name = "id_nisum_user")
    private NisumUser nisumUser;

    /**
     * Phone number
     */
    @Column(name = "phone_number")
    private String number;

    /**
     * City code
     */
    @Column(name = "city_code")
    private String cityCode;

    /**
     * Country code
     */
    @Column(name = "country_code")
    private String countryCode;



}

