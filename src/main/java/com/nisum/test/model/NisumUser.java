package com.nisum.test.model;

import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Nisum User
 */
@Data
@Entity
@Table(name = "nisum_user")
public class NisumUser {
    /**
     * User Id
     */
    @Id
    @Column(name = "id")
    @Type(type="uuid-char")
    @GeneratedValue
    private UUID id;

    /**
     * Username
     */
    @Column(name = "name")
    private String name;

    /**
     * Password
     */
    @Column(name = "email")
    private String email;

    /**
     * Created date
     */
    @Column(name = "created")
    private Date created;

    /**
     * Modified date
     */
    @Column(name = "modified")
    private Date modified;

    /**
     * Last login date
     */
    @Column(name = "last_login")
    private Date lastLogin;

    /**
     * Access token
     */
    @Column(name = "token")
    private String token;

    /**
     * is a user active
     */
    @Column(name = "is_active")
    private boolean isActive = true;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "nisumUser")
    private List<Phone> phones;

    @PrePersist
    protected void onCreate(){
        this.created = new Date();
        this.lastLogin = new Date();
    }

    @PreUpdate
    protected void onUpdate(){
        this.modified = new Date();
    }
}

