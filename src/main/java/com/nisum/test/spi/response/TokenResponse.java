package com.nisum.test.spi.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Data
public class TokenResponse {

    @ApiModelProperty(position = 1,name = "token",value = "Token",example = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJDb3RvLkN1YW50aWNhLkpXVCIsInN1YiI6InNkc2RmIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImlhdCI6MTYyMjg0MTY4OSwiZXhwIjoxNjIyODQyMjg5fQ.qkzoc_Yk09phC7XAcH6lCUFll2wU1gSAZi6tK7yC5pEWgBTvkDAVJGFSMosC0L1b67VQEd1oRCNqRYrftUPolA")
    private String token;

    @ApiModelProperty(position = 2,name = "expirationDate",value = "expirationDate",example = "")
    private Date expirationDate;

    @ApiModelProperty(position = 3,name = "issuedAt",value = "issuedAt",example = "")
    private Date issuedAt;

    @ApiModelProperty(position = 4,name = "tokenType", value = "TokenType",example = "Bearer")
    private String tokenType;

    @ApiModelProperty(position = 5,name = "refreshToken",value = "RefreshToken",example = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJDb3RvLkN1YW50aWNhLkpXVCIsInN1YiI6InNkc2RmIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImlhdCI6MTYyMjg0MTY4OSwiZXhwIjoxNjIyODQyMjg5fQ.qkzoc_Yk09phC7XAcH6lCUFll2wU1gSAZi6tK7yC5pEWgBTvkDAVJGFSMosC0L1b67VQEd1oRCNqRYrftUPolA")
    private String refreshToken;

}
