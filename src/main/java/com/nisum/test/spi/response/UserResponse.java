package com.nisum.test.spi.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Data
public class UserResponse {
    @ApiModelProperty(position = 1, name = "id", value = "id", example = "")
    private UUID id;

    @ApiModelProperty(position = 2, name = "created", value = "created", example = "")
    private String created;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(position = 3, name = "modified", value = "modified", example = "")
    private String modified;

    @ApiModelProperty(position = 4, name = "last_login", value = "last_login", example = "")
    private String last_login;

    @ApiModelProperty(position = 5, name = "token", value = "token", example = "")
    private String token;

    @ApiModelProperty(position = 6, name = "isactive", value = "isactive", example = "")
    private String isactive;
}
