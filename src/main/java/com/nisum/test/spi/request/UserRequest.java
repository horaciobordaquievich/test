package com.nisum.test.spi.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nisum.test.spi.constraint.CustomEmailValidator;
import com.nisum.test.spi.constraint.PasswordValidator;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class UserRequest {
    @NotBlank(message = "name must no be blank")
    @ApiModelProperty(position = 1,name = "name",value = "name",example = "Juan Rodriguez")
    private String name;

    @NotBlank(message = "email must no be blank")
    @ApiModelProperty(position = 2,name = "email",value = "email",example = "juan@gmail.com")
    @CustomEmailValidator
    private String email;

    @ApiModelProperty(position = 3,name = "password",value = "password",example = "Password123")
    @PasswordValidator
    private String password;

    @ApiModelProperty(position = 4,name = "phones",value = "phones",example = "")
    private List<PhoneRequest> phones;

    @JsonIgnore
    private String token;
}
