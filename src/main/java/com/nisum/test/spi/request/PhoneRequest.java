package com.nisum.test.spi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PhoneRequest {
    @NotNull
    @ApiModelProperty(position = 1, name = "number", value = "number", example = "1234567")
    private String number;

    @NotNull
    @ApiModelProperty(position = 2, name = "cityCode", value = "cityCode", example = "1")
    private String cityCode;

    @NotNull
    @ApiModelProperty(position = 3, name = "countryCode", value = "countryCode", example = "57")
    private String countryCode;
}
