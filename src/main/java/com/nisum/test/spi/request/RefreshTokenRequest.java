package com.nisum.test.spi.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode
@ApiModel(value = "RefreshToken", description = "Datos necesarios para obtener un nuevo token a partir de un refresh-token")
public class RefreshTokenRequest {
    @ApiModelProperty(position = 1, name = "refreshToken", value = "RefreshToken", example = "eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwiaXNzIjoiQ290by5DdWFudGljYS5KV1QiLCJqdGkiOiIwNzcwYjU1YS1jOGUwLTQ0NjktODUxNy04MTIzODQ3YWExNTAiLCJpYXQiOjE2MjMyNjg0NDcsImV4cCI6MTYyMzI2OTA0N30.ZufbtPIAOdmaKk8GehdCe94T0e9UkqIIQghrsbgpEvTK79h8hAouKSr2fhXel9FNm-DhE_w0NwI4QZp13IgFuw", required = true)
    @NotNull(message = "No puede ser nulo el refreshToken")
    private String refreshToken;

}
