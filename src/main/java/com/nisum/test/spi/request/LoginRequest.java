package com.nisum.test.spi.request;

import com.nisum.test.spi.constraint.PasswordValidator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode
@ApiModel(value = "LoginRequest", description = "Datos necesarios para Loguearse")
public class LoginRequest {
    @ApiModelProperty(position = 1, name = "user", value = "Username", example = "username", required = true)
    @NotNull(message = "username cannot be null")
    private String username;

    @ApiModelProperty(position = 2, name = "password", value = "Password", example = "Password123", required = true)
    @PasswordValidator
    private String password;
}
