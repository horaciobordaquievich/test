package com.nisum.test.spi.constraint;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Pattern(regexp="^(?=.{5,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]{2,}).*$", message="Password should have at least one capital letter, one or mor lowers letters and two numbers")
@Target( { FIELD, METHOD, PARAMETER, CONSTRUCTOR, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = {})
public @interface PasswordValidator {
    String message() default "Please provide a valid password";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
