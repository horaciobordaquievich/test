package com.nisum.test.service;

import com.nisum.test.spi.request.LoginRequest;
import com.nisum.test.spi.request.UserRequest;
import com.nisum.test.spi.response.UserResponse;

import java.util.List;

/**
 * User Service
 */
public interface UserService {
    /**
     *
     * @param userRequest {@link UserRequest} to crate a User
     * @param token to save with the user
     * @return
     */
    UserResponse createUser(UserRequest userRequest, String token);

    /**
     *
     * @return a {@link List} of {@link UserResponse}
     */
    List<UserResponse> listUsers();

    /**
     *
     * @param loginRequest to validate on AD for example
     * @return
     */
    boolean validateUser(LoginRequest loginRequest);
}
