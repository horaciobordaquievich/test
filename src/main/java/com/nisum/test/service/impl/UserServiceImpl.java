package com.nisum.test.service.impl;

import com.nisum.test.exception.BadRequestException;
import com.nisum.test.mapping.NisumUserMapper;
import com.nisum.test.model.NisumUser;
import com.nisum.test.repository.UserRepository;
import com.nisum.test.service.UserService;
import com.nisum.test.spi.request.LoginRequest;
import com.nisum.test.spi.request.UserRequest;
import com.nisum.test.spi.response.UserResponse;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Data
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private NisumUserMapper nisumUserMapper;

    @Autowired
    private UserRepository userRepository;

    @Transactional
    @Override
    public UserResponse createUser(UserRequest userRequest, String token) {
        log.info("checking user with email " + userRequest.getEmail());
        if(getUserRepository().findByEmail(userRequest.getEmail()).isPresent()){
            throw new BadRequestException("El correo "+userRequest.getEmail() + " ya esta registrado");
        }

        userRequest.setToken(token);
        NisumUser nisumUser = getNisumUserMapper().userRequestToNisumUser(userRequest);
        log.info("saving user {}",nisumUser);
        getUserRepository().save(nisumUser);

        return getNisumUserMapper().nisumUserToUserResponse(nisumUser);
    }

    @Override
    public List<UserResponse> listUsers() {
        return getNisumUserMapper().nisumUserListToUserResponseList(getUserRepository().findAll());
    }

    /**
     * @param loginRequest to validate on AD for example
     * @return
     */
    @Override
    public boolean validateUser(LoginRequest loginRequest) {
//        at this point will do the process to validate the login data, for example invoke a AD Service.
//        in this case, will return always true
        return true;
    }
}
