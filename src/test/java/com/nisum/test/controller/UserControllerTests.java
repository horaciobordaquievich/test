package com.nisum.test.controller;

import com.nisum.test.exception.BadRequestException;
import com.nisum.test.service.UserService;
import com.nisum.test.spi.request.UserRequest;
import com.nisum.test.spi.response.UserResponse;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTests {

    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }
    @Mock
    private UserService service;

    @InjectMocks
    private UserController controller;

    @Test
    public void create_valid_user(){
        UserRequest userRequest = new UserRequest();
        userRequest.setEmail("test@domain.cl");
        userRequest.setPassword("Password123");
        userRequest.setName("test");

        UserResponse expectedUserResponse = mock(UserResponse.class);
        String token = "token";
        when(service.createUser(userRequest,token)).thenReturn(expectedUserResponse);

        ResponseEntity<UserResponse> userResponse = controller.createUser(userRequest,token);

        Assertions.assertThat(userResponse).isNotNull();
        Assertions.assertThat(userResponse.getBody()).isEqualTo(expectedUserResponse);
    }

    @Test
    public void create_user_invalid_password() throws BadRequestException{
        UserRequest userRequest = new UserRequest();
        userRequest.setEmail("test@domain.cl");
        userRequest.setPassword("Password1");
        userRequest.setName("test");

        Set<ConstraintViolation<UserRequest>> violations = validator.validate(userRequest);

        assert(violations.size()>1);
    }

    @Test
    public void create_user_invalid_email() throws BadRequestException{
        UserRequest userRequest = new UserRequest();
        userRequest.setEmail("test@domain.");
        userRequest.setPassword("Password11");
        userRequest.setName("test");

        Set<ConstraintViolation<UserRequest>> violations = validator.validate(userRequest);

        assert(violations.size()>1);
    }

    @Test(expected = BadRequestException.class)
    public void create_user_existing_email() throws BadRequestException{
        UserRequest userRequest = new UserRequest();
        userRequest.setEmail("test@domain.com");
        userRequest.setPassword("Password11");
        userRequest.setName("test");
        String token = "token";

        when(service.createUser(userRequest,token)).thenThrow(new BadRequestException());

        controller.createUser(userRequest,token);
    }
}
